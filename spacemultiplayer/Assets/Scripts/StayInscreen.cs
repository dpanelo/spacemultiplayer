﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class StayInscreen : MonoBehaviour {

	private SpriteRenderer sr;
	//private Rigidbody2D rb;
	private Camera main_camera;

	void Awake(){
		//rb = GetComponent<Rigidbody2D> ();
		sr = GetComponent<SpriteRenderer> ();
		main_camera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
	}

	void LateUpdate () {
		var cameraBottomLeft = main_camera.ViewportToWorldPoint (Vector3.zero);
		var cameraTopRight = main_camera.ViewportToWorldPoint (Vector3.one);
		float x = transform.position.x;
		float y = transform.position.y;

		if (transform.position.x - sr.bounds.extents.x < cameraBottomLeft.x) {
			x = cameraBottomLeft.x + sr.bounds.extents.x;
		}
		if (transform.position.x + sr.bounds.extents.x > cameraTopRight.x){
			x = cameraTopRight.x - sr.bounds.extents.x;
		}
		if(transform.position.y + sr.bounds.extents.y > cameraTopRight.y){
			y = cameraTopRight.y - sr.bounds.extents.y;
		}
		if (transform.position.y - sr.bounds.extents.y < cameraBottomLeft.y) {
			y = cameraBottomLeft.y + sr.bounds.extents.y;
		}

		transform.position = new Vector3 (x, y, transform.position.z);

	}
}
