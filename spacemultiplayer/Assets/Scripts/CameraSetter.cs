﻿using UnityEngine;
using System.Collections;

public class CameraSetter : MonoBehaviour {

	private SpawnController controller;

	// Use this for initialization
	void Start () {
	}

	public void SetSettings(){
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<SpawnController> ();
		controller.SetCamera ();
	}
		
}
