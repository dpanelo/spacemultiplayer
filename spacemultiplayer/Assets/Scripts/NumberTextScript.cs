﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NumberTextScript : MonoBehaviour {

	public Text childText;
	private Slider thisSlider;
	private MenuController controller;
	// Use this for initialization
	void Start () {
		thisSlider = GetComponent<Slider> ();
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<MenuController> ();
		thisSlider.onValueChanged.AddListener (delegate {
			ChangeTextValue ();
		});
	}

	public void ChangeTextValue(){
		childText.text = ((int)thisSlider.value).ToString();
		controller.SetNumberOfPlayers ((int)thisSlider.value);
	}


}
