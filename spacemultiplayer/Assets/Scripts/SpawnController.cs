﻿using UnityEngine;
using System.Collections;


public class SpawnController : MonoBehaviour {

	private CameraControl control;
	private GameStateController stateController;

	public Vector3[] initialSpawnPoints;
	public GameObject shipTemplate;
	public System.Collections.Generic.List<Transform> ships;
	public int[] playerLives;
	public bool[] doNotRespawn;
	public bool[] isAlive;
	public int startingLives;
	public bool gameStarted;


	void Awake(){
		DontDestroyOnLoad (this.gameObject);
		stateController = GetComponent<GameStateController> ();
		shipTemplate = Resources.Load<GameObject>("Player1");
		Debug.Assert (shipTemplate != null, "Ship template is null");
		initialSpawnPoints = new Vector3[4];
	}

	void Start(){
	}
		
	void Update(){
		if (gameStarted) {
			for (int i = 0; i < isAlive.Length; i++) {
				if (CheckIfPlayerDeadAndCanRespawn (i)) {
					Vector3 point = GetNewSpawnPoint ();
					Spawn ((i + 1), point);
				}
			}
		}
	}

	public void SubtractLife(int player){
		var playerIndex = player - 1;
		isAlive [playerIndex] = false;
		var currentLives = playerLives [playerIndex];
		playerLives [playerIndex] = currentLives - 1;
		Debug.Log (isAlive [playerIndex]);
		Debug.Log (playerLives [playerIndex]);
		Debug.Log (doNotRespawn [playerIndex]);
		if (playerLives [playerIndex] <= 0) {
			doNotRespawn [playerIndex] = true;
		}
		if (OnlyOneRemains ()) {
			int winner = GetWinner ();
			GameEnd (winner);
		}
	}

	public void RemoveTransform(Transform trans){
		control.m_Targets.Remove (trans);
		ships.Remove (trans);
	}

	public void AddTransform(Transform shipTransform){
		Transform trans = shipTransform;
		if(trans != null && trans.GetType() == typeof(Transform))
		control.m_Targets.Add (trans);
	}

	public void Spawn(int number, Vector3 point){
		GameObject newShip = (GameObject.Instantiate (shipTemplate, point, Quaternion.identity) as GameObject);
		Transform newShipTransform = newShip.transform;
		newShip.GetComponent<ShipSetter> ().SetShipNumber (number);
		isAlive [number - 1] = true;
		if (newShipTransform != null && newShipTransform.GetType() == typeof(Transform)) {
			this.AddTransform (newShipTransform);
		}
	}
		

	public void SetAmountOfPlayers(int number){
		playerLives = new int[number];
		doNotRespawn = new bool[number];
		isAlive = new bool[number];
		Debug.Log ("playerLives: " + playerLives.Length);
		Debug.Log ("doNotRespawn: " + doNotRespawn.Length);
		Debug.Log ("IsAlive: " + isAlive.Length);

		for(int i = 0; i < number; i++){
			playerLives[i] = startingLives;
			Debug.Log (playerLives [i]);
			doNotRespawn [i] = false;
			isAlive [i] = false;
		}
	}

	public void SetStartingLives(int lives){
		startingLives = lives;
	}

	bool CheckIfPlayerDeadAndCanRespawn(int index){
		bool playerDead = !(isAlive [index]);
		bool canRespawn = !(doNotRespawn [index]);

		return playerDead && canRespawn;
	}

	Vector3 GetNewSpawnPoint(){
		Vector3 middlePoint = GetMiddlePoint ();
		Vector3 newPoint = Vector3.zero;

		foreach (Transform ship in ships) {
			newPoint += ship.position - middlePoint;
		}

		return newPoint;
	}

	Vector3 GetMiddlePoint(){
		Vector3 middlePoint = Vector3.zero;
		int i = 0;
		foreach (Transform ship in ships) {
			middlePoint += ship.position;
			i++;
		}
		if (i > 0) {
			middlePoint = middlePoint / i;
		}

		return middlePoint;
	}

	public void GameStart(){
		gameStarted = true;
	}

	public void GameEnd(int winner){
		gameStarted = false;
		stateController.GameEnded (winner);
	}

	public int GetWinner(){
		int winner = 0;
		for (int i = 0; i < playerLives.Length; i++) {
			if (playerLives [i] > 0) {
				winner = i + 1;
			}
		}

		return winner;
	}

	bool OnlyOneRemains ()
	{
		int playersWithLives = 0;

		for (int i = 0; i < doNotRespawn.Length; i++) {
			if (!doNotRespawn [i]) {
				playersWithLives++;
			}
		}

		return playersWithLives < 2;
	}

	public void InitialSpawn(int players){
		for (int i = 0; i < players; i++) {
			Vector3 aPoint = initialSpawnPoints[i];
			Debug.Log (i + 1);
			Spawn((i + 1), aPoint);

		}
	}

	public void SetCamera()	{
		control = GameObject.FindGameObjectWithTag ("Rig").GetComponent<CameraControl> ();
		if (control != null)
			Debug.Log ("Camera found");
	}

	public void SetInitialSpawnPoints(){
		for (int i = 0; i < initialSpawnPoints.Length; i++) {
			initialSpawnPoints [i] = GameObject.FindGameObjectWithTag("SpawnPoint" + (i+1)).transform.position;
		}
	}
		
}
