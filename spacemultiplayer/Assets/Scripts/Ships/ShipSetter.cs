﻿using UnityEngine;
using System.Collections;

public class ShipSetter : MonoBehaviour {

	private Movement shipMovement;
	private HitPoints shipHitPoints;
	private Shoot laser;
	private SpriteRenderer sr;
	private Rigidbody2D rb;
	private Color[] colors;

	void Awake(){
		shipMovement = GetComponent<Movement> ();
		shipHitPoints = GetComponent<HitPoints> ();
		laser = GetComponentInChildren<Shoot> ();
		sr = GetComponent<SpriteRenderer> ();
		rb = GetComponent<Rigidbody2D> ();
		//I don't like this here, but if I put it in Start() sometimes colors = null
		colors = new Color[]{
			Color.white,
			Color.magenta,
			Color.red,
			Color.green
		};
	}

	void Start(){
		
	}

	public void SetShipNumber(int number){
		sr.color = colors [number - 1];
		shipMovement.SetPlayerNumber (number);
		shipHitPoints.SetPlayerNumber (number);
		laser.SetPlayerNumber (number);

	}

	void ShipActive(bool active){
		shipMovement.CanMove (active);
		shipHitPoints.SetVulnerable (active);
		laser.CanShoot (active);
	}

	public void SetInvisible(){
		sr.enabled = false;
		rb.Sleep ();
	}

	public void SetVisible(){
		sr.enabled = true;
		rb.WakeUp ();
	}
}
