﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {
	public GameObject shot;
	public int playerNumber;
	public float fireFrequency;

	private float timer;
	private GameObject firedBullet;
	private GameObject parent;
	public bool canShoot;

	void Awake(){
		parent = transform.parent.gameObject;
		shot = Resources.Load<GameObject> ("Shot");
	}

	void Start(){
		timer = 0.0f;
	}

	void Update(){
		if (Input.GetAxis ("Player" + playerNumber + " Shoot") >= 1.0f && canShoot) {
			ShootLaser ();
		}
		if (timer > 0.0f) {
			timer = timer - Time.deltaTime;
		}
	}

	//TODO: crear sonido de laser
	//TODO: resolver problema de dispararse a uno mismo
	private void ShootLaser(){
		if (timer <= 0.0f) {
			firedBullet = (GameObject.Instantiate (shot, transform.position, parent.transform.rotation) as GameObject);
			firedBullet.GetComponent<ShotDestroy>().SetParent(parent);
			timer = fireFrequency;
		}
	}

	public void CanShoot(bool enabled){
		canShoot = enabled;
	}

	public void SetPlayerNumber(int player){
		playerNumber = player;
	}
}
