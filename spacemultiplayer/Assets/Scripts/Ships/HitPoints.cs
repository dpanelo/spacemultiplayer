﻿using UnityEngine;
using System.Collections;

public class HitPoints : MonoBehaviour {

	public int playerNumber;

	private SpawnController gameController;
	private int hitPoints, lives;
	public bool isVulnerable;

	void Awake(){
		gameController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<SpawnController>();
	}

	// Use this for initialization
	void Start () {
		hitPoints = 5;
		//TODO: Extraer las vidas y mandarlas al LivesController
		lives = 3;
	}

	//TODO: Crear sonido de destrucción de nave
	public void TakeDamage(){
		if (isVulnerable && hitPoints > 0) {
			hitPoints--;
		}
		Debug.Log ("Hit Points = " + hitPoints);
		if (hitPoints <= 0) {
			DestroyShip ();
		}
	}

	public void SetHitPointsTo(int newValue){
		hitPoints = newValue;
	}

	private void DestroyShip(){
		gameController.SubtractLife (playerNumber);
		gameController.RemoveTransform (transform);
		GameObject.Destroy (gameObject);
	}

	public void SetPlayerNumber(int number){
		playerNumber = number;
	}

	public void SetVulnerable(bool enabled){
		isVulnerable = enabled;
	}
}
