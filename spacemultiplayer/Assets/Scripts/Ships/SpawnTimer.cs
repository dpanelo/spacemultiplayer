﻿using UnityEngine;
using System.Collections;

public class SpawnTimer : MonoBehaviour {

	public float spawnTime;
	private float timer;

	private bool active;
	private SpawnController controller;
	private ShipSetter shipSetter;

	void Awake(){
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<SpawnController> ();
		shipSetter = GetComponent<ShipSetter> ();
	}

	// Use this for initialization
	void Start () {
		timer = 0.0f;
		active = false;
		shipSetter.SetInvisible ();
	}
	
	// Update is called once per frame
	void Update () {

		if (!active) {
			timer = timer + Time.deltaTime;
			if (timer >= spawnTime) {

				active = true;
				shipSetter.SetVisible ();
				//controller.AddTransform(transform);
			}
		}
	}
}
