﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour {

	public float acceleration;
	public float turningSpeed;
	public int playerNumber;
	public bool canMove;

	private bool accelerating;

	private Rigidbody2D rb;

	void Awake(){
		rb = GetComponent<Rigidbody2D> ();

	}

	// Use this for initialization
	void Start () {
		accelerating = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(canMove){
			if (Input.GetAxis ("Player" + playerNumber + " Rotate Left") >= 1.0f) {
				transform.Rotate (Vector3.back, (-1) * turningSpeed * Time.deltaTime);
			}
			if (Input.GetAxis ("Player" + playerNumber + " Rotate Right") >= 1.0f) {
				transform.Rotate (Vector3.back, turningSpeed * Time.deltaTime);
			}
			if (Input.GetAxis ("Player" + playerNumber + " Accelerate") >= 1.0f) {
				accelerating = true;
			} else {
				accelerating = false;
			}
		}
	}

	void FixedUpdate(){
		if (accelerating)
			rb.AddForce (transform.up * acceleration);
	}

	public void SetPlayerNumber(int number){
		playerNumber = number;
	}

	public void CanMove(bool enabled){
		canMove = enabled;
	}
}
