﻿using UnityEngine;
using System.Collections;

public class ShotTravel : MonoBehaviour {

	public float velocity;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (transform.up * velocity * Time.deltaTime, Space.World);
	}
}
