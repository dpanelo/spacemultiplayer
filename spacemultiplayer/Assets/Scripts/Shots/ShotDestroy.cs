﻿using UnityEngine;
using System.Collections;

public class ShotDestroy : MonoBehaviour {

	private SpriteRenderer sr;
	private GameObject whoShotMe;

	void Awake(){
		sr = GetComponent<SpriteRenderer> ();
	}

	void Update(){
		if(!sr.isVisible){
			Destroy(gameObject);
		}
	}

	//Pasarle el gameObject de la nave
	//TODO: crear sonido de golpe contra nave
	void OnTriggerEnter2D(Collider2D other){
		Debug.Log ("Trigger!");
		if (other.gameObject.CompareTag ("Player") && other.gameObject.GetInstanceID() != whoShotMe.GetInstanceID()) {
			other.gameObject.SendMessage ("TakeDamage");
			GameObject.Destroy (gameObject);
		}
	}


	public void SetParent(GameObject parent){
		whoShotMe = parent;
	}
}
