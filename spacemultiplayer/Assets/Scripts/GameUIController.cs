﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameUIController : MonoBehaviour {

	private GameObject pausePanel;
	private GameObject winPanel;
	private Text winPanelText;
	private bool isPaused;

	void Start(){
		isPaused = false;
	}


	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			this.SetPause ();
		}
	}

	private void SetPause(){
		isPaused = !isPaused;
		if (isPaused) {
			Time.timeScale = 0.0f;
		} else {
			Time.timeScale = 1.0f;
		}

		Debug.Log ("PausePanel != null == " + pausePanel != null);
		pausePanel.SetActive (!pausePanel.activeSelf);
	}

	public void FindPanels (){
		var canvas = GameObject.FindGameObjectWithTag ("Canvas");
		//GameObject.FindGameObjectWithTag() cannot find inactive GameObjects
		pausePanel = canvas.transform.FindChild ("PauseMenu").gameObject;
		winPanel = canvas.transform.FindChild ("WinPanel").gameObject;
		winPanelText = winPanel.transform.FindChild ("Winner").GetComponent<Text> ();
		Debug.Log ("Looking for Panels");
		Debug.Assert (pausePanel != null);
		Debug.Assert (winPanel != null);
	}

	public void UpdateWinScreen(int winner){
		winPanelText.text = "Player " + winner + " wins!!!!";
	}

	public void ShowWinScreen(){
		winPanel.SetActive (true);
	}
}
