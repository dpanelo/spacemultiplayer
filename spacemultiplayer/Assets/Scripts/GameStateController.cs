﻿using UnityEngine;
using System.Collections;

public class GameStateController : MonoBehaviour {
	private SpawnController spawn;
	private GameUIController UI;

	void Awake(){
		spawn = GetComponent<SpawnController> ();
		UI = GetComponent<GameUIController> ();
	}
		

	public void GameEnded(int winner){
		UI.UpdateWinScreen (winner);
		UI.ShowWinScreen ();
	}

	public void PrepareToStart(int players, int lives){
		//Call order is IMPORTANT
		UI.FindPanels ();
		spawn.SetCamera ();
		spawn.SetInitialSpawnPoints ();
		spawn.SetStartingLives (lives);
		spawn.SetAmountOfPlayers (players);
		spawn.InitialSpawn (players);
	}


}
