﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	private GameStateController state;

	private Slider playerSlider;
	private int numberOfPlayers;
	private int numberOfLives;

	void Awake(){
		DontDestroyOnLoad (gameObject);
		state = GetComponent<GameStateController> ();
	}

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (this);
		numberOfPlayers = 2;
		numberOfLives = 1;
		SceneManager.sceneLoaded += ActuallyStart;
	}

	public void SetNumberOfPlayers(int number){
		numberOfPlayers = number;
		Debug.Log (numberOfPlayers);
	}

	public int GetLives(){
		return numberOfLives;
	}

	public int GetPlayers(){
		return numberOfPlayers;
	}

	public void GameStart(){
		SceneManager.LoadScene (1);

	}

	void ActuallyStart(Scene escenaCargada, LoadSceneMode modo){
		state.PrepareToStart (numberOfPlayers, numberOfLives);
	}
		
}
